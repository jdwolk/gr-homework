const createError = require("http-errors");
const express = require("express");
const API = require("express-openapi");
const swaggerUi = require("swagger-ui-express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

const apiDoc = require("./src/api/v1/apiDoc");
const recordsService = require("./src/api/v1/services/records");

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

API.initialize({
  app,
  apiDoc,
  dependencies: {
    recordsService,
  },
  paths: "./src/api/v1/paths",
  pathsIgnore: new RegExp(".(spec|test|makeGetRecordsEndpoint)$"),
});

// NOTE: if the SEED_RECORDS env var is passed this will load the records
// from data/comma-separated.csv into the DB
(() => {
  if (!process.env.SEED_RECORDS) {
    return;
  }

  const ParseRecords = require("./src/lib/recordParsing");
  const RecordsInDB = require("./src/db/records");

  const file = "./data/comma-separated.csv";
  ParseRecords.fromFiles([file]).values().forEach(RecordsInDB.add);
})();

app.use(
  "/api-documentation",
  swaggerUi.serve,
  swaggerUi.setup(null, {
    swaggerOptions: {
      url: "http://localhost:3000/api-docs",
    },
  })
);

// catch 404 and forward to error handler
app.use((_req, _res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;

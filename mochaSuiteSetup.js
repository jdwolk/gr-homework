/* eslint-disable no-undef,no-unused-vars */
const R = require("ramda");
const chai = require("chai");
chai.use(require("sinon-chai"));

const DB = require("./src/util/db");

const testTags = (test) => {
  /* eslint-disable-next-line */
  const tagsRegexp = /tags\:\ (.*)\)/g;
  const tagsMatches = tagsRegexp.exec(test.title);
  const tags = tagsMatches && tagsMatches[1] && R.split(",", tagsMatches[1]);
  return tags;
};

const _taggedWith = (tags) => (tag) => {
  if (!tags) {
    return false;
  }

  return R.includes(tag, tags);
};

beforeEach(function () {
  // NOTE: for now I'm erring on the side of caution and resetting
  // the DB between each test.
  //
  // If we want to be more selective about which tests
  // reset the DB we can add '(tags: db)' to test descriptions +
  // the following here:
  //
  // const taggedWith = _taggedWith(this.currentTest.tags)
  //
  // if (taggedWith('db')) { ... }
  //
  DB.reset();
});
/* eslint-enable no-undef,no-unused-vars*/

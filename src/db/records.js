// NOTE: this module is sort of a DAO for accessing records from the DB - it
// should be preferred over accessing `DB` directly

const { Records } = require("../lib/records");
const DB = require("../util/db");

const RECORDS_KEY = "records";

const add = (record) => {
  DB.update(RECORDS_KEY, Records.addRecord(record));
};

const all = () => {
  return DB.get(RECORDS_KEY);
};

module.exports = {
  all,
  add,
};

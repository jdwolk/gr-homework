const path = require("path");

const dataFile = (fileName) => {
  return path.resolve(path.join(__dirname, "..", "..", "data", fileName));
};

module.exports = {
  dataFile,
};

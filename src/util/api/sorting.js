const Result = require("crocks/Result");
const { Ok } = Result;

const { SORT_DIRECTIONS } = require("../../lib/constants");
const { capitalize } = require("../formatting");
const { as, checkIsOneOf } = require("../results");

const { Asc, Desc } = SORT_DIRECTIONS;

const getSortDirection = ({ props }) => {
  const sortDirection = capitalize(props.sortDirection);
  return checkIsOneOf({
    vals: [Asc, Desc],
    given: sortDirection,
    defaultVal: Asc,
  });
};

const getSortBy = ({ props, fields, defaultSortField }) => {
  const sortBy = capitalize(props.sortBy);
  return checkIsOneOf({
    vals: fields,
    given: sortBy,
    defaultVal: defaultSortField,
  });
};

const sort = ({ fields, defaultSortField }) => ({ props }) => {
  return Ok({ props, fields, defaultSortField })
    .chain(as("sortDirection", getSortDirection))
    .chain(as("sortBy", getSortBy));
};

module.exports = sort;

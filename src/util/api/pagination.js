const R = require("ramda");
const Result = require("crocks/Result");
const { Ok } = Result;

const { as } = require("../results");

const DEFAULT_ITEMS_PER_PAGE = 100;

const getPage = ({ props }) => {
  const { page } = props;

  return Ok(page || 1);
};

const getPerPage = ({ props }) => {
  const { perPage } = props;

  return Ok(perPage || DEFAULT_ITEMS_PER_PAGE);
};

const getTotalItems = ({ allItems }) => {
  return Ok(R.length(allItems));
};

const getTotalPages = ({ totalItems, perPage }) => {
  const totalPages = Math.ceil(totalItems / perPage);

  return Ok(totalPages);
};

const getPaginatedItems = ({ allItems, page, perPage }) => {
  if (perPage >= R.length(allItems)) {
    return Ok(allItems);
  }
  if (page === 0) {
    return Ok(allItems);
  }

  const start = (page - 1) * perPage;
  const end = page * perPage;
  return Ok(R.slice(start, end, allItems));
};

const present = (args) => {
  return Ok(
    R.pick(["page", "perPage", "totalItems", "totalPages", "items"], args)
  );
};

const paginate = ({ getItems }) => (accumulated) => {
  return Ok(accumulated)
    .chain(as("page", getPage))
    .chain(as("perPage", getPerPage))
    .chain(as("allItems", getItems))
    .chain(as("totalItems", getTotalItems))
    .chain(as("totalPages", getTotalPages))
    .chain(as("items", getPaginatedItems))
    .chain(present);
};

module.exports = paginate;

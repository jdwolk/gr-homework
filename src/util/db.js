// NOTE: this module exports functions for interacting with an in-memory key value store

const { Records } = require("../lib/records");

const DB = () => {
  const initialState = () => ({
    records: Records.empty(),
  });

  // NOTE: this variable is effectively _global_ since
  // 1. the module is evaluated once when first require'd
  // 2. the returned functions close over the value
  let _db = initialState();

  const reset = () => {
    _db = initialState();
  };

  const get = (key) => _db[key];

  const set = (key, val) => (_db[key] = val);

  const update = (key, fn) => {
    const previousVal = get(key) || {};
    const newVal = fn(previousVal);
    _db[key] = newVal;
    process.env.DEBUG && console.log("DB: ", JSON.stringify(_db, null, 2));
  };

  return {
    set,
    get,
    update,
    reset,
  };
};

module.exports = DB();

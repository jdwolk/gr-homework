const R = require("ramda");
const Result = require("crocks/Result");
const { Ok, Err } = Result;

// NOTE: in general this is sort of "cheating" the Result type
// but it can be useful in, i.e., tests
const isOk = (result) => {
  return result.either(
    () => false,
    () => true
  );
};

const getOrElse = R.curry((defaultVal, result) => {
  return result.either(
    () => defaultVal,
    (val) => val
  );
});

const failWith = (message) => {
  return Err({ message });
};

// NOTE: this lets you accumulate results across .chain calls
// since JS doesn't have 'do' notation - otherwise
// we'd have a bunch of nested fn calls in order to access
// earlier results.
//
// Ex:
//
// Ok({})
//   .chain(as('foobar', producesFoobar))
//   .chain(as('baz', producesBaz))
//
// => Ok({ foobar: ..., baz: ... })
//
const as = R.curry((key, resultFn, obj) => {
  return resultFn(obj).chain((val) => Ok({ ...obj, ...{ [key]: val } }));
});

const checkIsOneOf = ({ vals, given, defaultVal }) => {
  if (!given) {
    return Ok(defaultVal);
  }

  if (R.includes(given, vals)) {
    return Ok(given);
  }

  return failWith(`'${given}' was not one of: '${vals}'`);
};

module.exports = {
  as,
  checkIsOneOf,
  failWith,
  getOrElse,
  isOk,
};

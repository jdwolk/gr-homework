const R = require("ramda");

const mapKeys = (fn, obj) => R.zipObj(R.map(fn, R.keys(obj)), R.values(obj));

module.exports = {
  mapKeys,
};

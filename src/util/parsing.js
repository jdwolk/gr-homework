/* eslint-disable-next-line */
const _syncParse = require("csv-parse/lib/sync");

const syncParse = (givenOptions) => {
  const { contents } = givenOptions;
  const options = {
    ...givenOptions,
    ...{
      trim: true,
      columns: true,
    },
  };
  return _syncParse(contents, options);
};

module.exports = {
  syncParse,
};

const { mapKeys } = require("./collections");

const dateFormatChoices = {
  mdyyyy: "M/d/yyyy",
};

const dateFormats = {
  default: dateFormatChoices.mdyyyy,
};

const onFirstLetter = (fn) => (str) => {
  const chars = str.split("");
  return [fn(chars[0])].concat(chars.slice(1)).join("");
};

const decapitalize = onFirstLetter((letter) => letter.toLowerCase());

const capitalize = onFirstLetter((letter) => letter.toUpperCase());

const lowerKeys = (obj) => {
  return mapKeys(decapitalize, obj);
};

module.exports = {
  capitalize,
  decapitalize,
  dateFormats,
  lowerKeys,
};

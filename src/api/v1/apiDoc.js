// TODO: keep constants / properties / fields in #components/schemas
// synchronized w/ actual data
const apiDoc = {
  openapi: "3.0.0",
  info: {
    title: "JD Wolk's GR Homework API",
    version: "0.0.1",
  },
  components: {
    schemas: {
      CreateRecord: {
        type: "object",
        required: ["line"],
        properties: {
          line: {
            description: "A single data line in any of the 3 formats supported",
            type: "string",
            example: "Atreides,Paul,muaddib@example.com,blue,694310400000",
          },
        },
      },
      CreateRecordFailed: {
        type: "object",
        properties: {
          message: {
            description:
              "A message explaining why the record could not be parsed or created",
            type: "string",
            example: "Could not determine delimiter type",
          },
        },
      },
      Record: {
        type: "object",
        required: [
          "firstName",
          "lastName",
          "email",
          "favoriteColor",
          "dateOfBirth",
        ],
        properties: {
          firstName: {
            type: "string",
            example: "Paul",
          },
          lastName: {
            type: "string",
            example: "Atreides",
          },
          email: {
            type: "string",
            example: "muaddib@example.com",
          },
          favoriteColor: {
            type: "string",
            example: "blue",
          },
          dateOfBirth: {
            type: "string",
            example: "694310400000",
          },
        },
      },
      Records: {
        type: "object",
        properties: {
          page: {
            type: "integer",
            description: "Page number used for paginating items",
            example: 1,
          },
          perPage: {
            type: "integer",
            description: "Number of items used per-page when paginating",
            example: 10,
          },
          totalItems: {
            type: "integer",
            description: "Total items returned by the query",
            example: 123,
          },
          totalPages: {
            type: "integer",
            description:
              "Total pages (w/ perPage # of items each page) returned by the query",
            example: 3,
          },
          items: {
            type: "array",
            items: {
              $ref: "#/components/schemas/Record",
            },
          },
        },
      },
    },
  },
  paths: {},
};

module.exports = apiDoc;

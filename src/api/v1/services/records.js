const addRecord = require("./records/addRecord");
const getRecords = require("./records/getRecords");

module.exports = {
  addRecord,
  getRecords,
};

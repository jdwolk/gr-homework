const { expect } = require("chai");

const addRecord = require("./addRecord");
const RecordsInDB = require("../../../../db/records");
const { FIELDS: F, DELIMITERS } = require("../../../../lib/constants");
const { makeRecord } = require("../../../../lib/dataGeneration");
const { isOk, getOrElse } = require("../../../../util/results");

const recordToLine = (
  { record, delimiter } = { delimiter: DELIMITERS.comma }
) => {
  const {
    [F.LastName]: lastName,
    [F.FirstName]: firstName,
    [F.Email]: email,
    [F.FavoriteColor]: favoriteColor,
    [F.DateOfBirth]: dateOfBirth,
  } = record;
  return [lastName, firstName, email, favoriteColor, dateOfBirth].join(
    delimiter
  );
};

describe("addRecord", () => {
  describe("given valid input", () => {
    it("successfuly returns the added record (tags: db)", () => {
      const record = makeRecord();

      const params = {
        line: recordToLine({ record }),
      };
      const result = addRecord(params);

      expect(isOk(result)).to.eq(true);
      expect(getOrElse(null, result)).to.eql(record);
    });

    it("adds the record to the DB (tags: db)", () => {
      const recordsBefore = RecordsInDB.all().values();
      expect(recordsBefore.length).to.eq(0);

      const params = {
        line: recordToLine({ record: makeRecord() }),
      };
      addRecord(params);

      const recordsAfter = RecordsInDB.all().values();
      expect(recordsAfter.length).to.be.greaterThan(0);
    });
  });

  it("fails with no 'line' parameter", () => {
    const params = {};
    const result = addRecord(params);

    expect(isOk(result)).to.eq(false);
  });

  it("fails when the delimiter is not recognized", () => {
    const unrecognizedDelimiter = "~";
    const record = makeRecord();
    const params = {
      line: recordToLine({ record, delimiter: unrecognizedDelimiter }),
    };
    const result = addRecord(params);

    expect(isOk(result)).to.eq(false);
  });

  it("fails when the line cannot be parsed with a known delimiter", () => {
    const params = {
      line: "bad,input",
    };
    const result = addRecord(params);

    expect(isOk(result)).to.eq(false);
  });
});

const { expect } = require("chai");
const { fake, stub } = require("sinon");

const { makeRecord } = require("../../../../lib/dataGeneration");
const { Records } = require("../../../../lib/records");
const { isOk } = require("../../../../util/results");
const RecordsInDB = require("../../../../db/records");
const getRecords = require("./getRecords");
const { FIELDS: F, SORT_DIRECTIONS } = require("../../../../lib/constants");
const { Asc } = SORT_DIRECTIONS;

describe("getRecords", () => {
  afterEach(() => {
    RecordsInDB.all.restore();
  });

  it("calls RecordsInDB.all", () => {
    const records = [makeRecord(), makeRecord()];
    const recordsInDB = Records.fromArray(records);
    const recordsInDBAll = fake.returns(recordsInDB);
    stub(RecordsInDB, "all").callsFake(recordsInDBAll);

    const props = {
      sortBy: F.Email,
      sortDirection: Asc,
    };

    getRecords(props);

    expect(recordsInDBAll).to.have.been.called;
  });

  it("is successful when correct props are passed", () => {
    const records = [makeRecord(), makeRecord()];
    const recordsInDB = Records.fromArray(records);
    const recordsInDBAll = fake.returns(recordsInDB);
    stub(RecordsInDB, "all").callsFake(recordsInDBAll);

    const props = {
      sortBy: F.Email,
      sortDirection: Asc,
    };

    const result = getRecords(props);

    expect(isOk(result)).to.eq(true);
  });

  it("fails when sortBy is unrecognized", () => {
    const records = [makeRecord(), makeRecord()];
    const recordsInDB = Records.fromArray(records);
    const recordsInDBAll = fake.returns(recordsInDB);
    stub(RecordsInDB, "all").callsFake(recordsInDBAll);

    const props = {
      sortBy: "foobar",
      sortDirection: Asc,
    };

    const result = getRecords(props);

    expect(isOk(result)).to.eq(false);
  });

  it("fails when sortDirection is unrecognized", () => {
    const records = [makeRecord(), makeRecord()];
    const recordsInDB = Records.fromArray(records);
    const recordsInDBAll = fake.returns(recordsInDB);
    stub(RecordsInDB, "all").callsFake(recordsInDBAll);

    const props = {
      sortBy: F.Email,
      sortDirection: "Nope",
    };

    const result = getRecords(props);

    expect(isOk(result)).to.eq(false);
  });
});

const R = require("ramda");
const Result = require("crocks/Result");
const { Ok } = Result;

const RecordsInDB = require("../../../../db/records");
const { SortBy } = require("../../../../lib/sortBy");
const { FIELDS } = require("../../../../lib/constants");
const { lowerKeys } = require("../../../../util/formatting");

const paginate = require("../../../../util/api/pagination");
const sort = require("../../../../util/api/sorting");

const F = FIELDS;

const getItems = ({ sortBy: field, sortDirection: direction }) => {
  const rawRecords = RecordsInDB.all().sortValues([SortBy(field, direction)]);
  const records = R.map(lowerKeys, rawRecords);

  return Ok(records);
};

const getRecords = (props) => {
  return Ok({ props })
    .chain(sort({ fields: R.keys(FIELDS), defaultSortField: F.LastName }))
    .chain(paginate({ getItems }));
};

module.exports = getRecords;

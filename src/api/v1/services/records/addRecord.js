const R = require("ramda");
const Result = require("crocks/Result");
const { Ok } = Result;

const RecordsInDB = require("../../../../db/records");
const { FIELDS: F } = require("../../../../lib/constants");
const {
  determineDelimiterType,
  parse,
} = require("../../../../lib/recordParsing");
const { as, failWith } = require("../../../../util/results");

const makeHeaderLine = (delimiter) => {
  return R.join(delimiter, [
    F.LastName,
    F.FirstName,
    F.Email,
    F.FavoriteColor,
    F.DateOfBirth,
  ]);
};

const checkLinePresent = ({ line }) => {
  return line
    ? Ok(line)
    : failWith(
        'Input should look like: { "line": "Wolk,JD,jd@example.com,blue,70658082152" }'
      );
};

const parseDelimiter = ({ line }) => {
  const delimiter = determineDelimiterType(line);
  return delimiter
    ? Ok(delimiter)
    : failWith("Could not determine delimiter type");
};

const parseRecord = ({ delimiter, line }) => {
  // Because we only get one line passed as input we need
  // to create a header line using the given delimiter
  // to make an actual record
  const headerLine = makeHeaderLine(delimiter);
  const lines = R.join("\n", [headerLine, line]);

  const records = parse(lines);
  if (!(records && records[0])) {
    return failWith(`Could not parse a record from the input '${line}'`);
  }

  return Ok(records[0]);
};

const addRecordToDB = ({ record }) => {
  RecordsInDB.add(record);

  return Ok(record);
};

const addRecord = (params) => {
  return Ok(params)
    .chain(as("line", checkLinePresent))
    .chain(as("delimiter", parseDelimiter))
    .chain(as("record", parseRecord))
    .chain(addRecordToDB);
};

module.exports = addRecord;

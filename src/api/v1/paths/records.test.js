const { expect } = require("chai");
const { fake } = require("sinon");
const { mockRequest, mockResponse } = require("mock-req-res");
const Result = require("crocks/Result");
const { Err, Ok } = Result;

const RecordsPaths = require("./records");
const { makeRecord } = require("../../../lib/dataGeneration");
const { lowerKeys } = require("../../../util/formatting");

describe("POST /records", () => {
  it("calls addRecord", () => {
    const addRecord = fake.returns(Ok("ok"));
    const recordsService = {
      addRecord,
    };

    const body = {
      lines: "",
    };

    const req = mockRequest({ body });
    const res = mockResponse();

    const subject = RecordsPaths(recordsService);
    subject.POST(req, res);

    expect(addRecord).to.have.been.calledWith(body);
  });

  it("returns 400 status when addRecord fails", () => {
    const addRecord = fake.returns(Err("oh no!"));
    const recordsService = {
      addRecord,
    };

    const body = {
      lines: "",
    };

    const req = mockRequest({ body });
    const res = mockResponse();

    const subject = RecordsPaths(recordsService);
    subject.POST(req, res);

    expect(res.status).to.have.been.calledWith(400);
  });

  describe("when addRecord succeeds", () => {
    it("returns 201 status", () => {
      const addRecord = fake.returns(Ok("ok"));
      const recordsService = {
        addRecord,
      };

      const body = {
        lines: "",
      };

      const req = mockRequest({ body });
      const res = mockResponse();

      const subject = RecordsPaths(recordsService);
      subject.POST(req, res);

      expect(res.status).to.have.been.calledWith(201);
    });

    it("returns json containing the added record", () => {
      const record = lowerKeys(makeRecord());
      const addRecord = fake.returns(Ok(record));
      const recordsService = {
        addRecord,
      };

      const body = {
        lines: "",
      };

      const req = mockRequest({ body });
      const res = mockResponse();

      const subject = RecordsPaths(recordsService);
      subject.POST(req, res);

      expect(res.json).to.have.been.calledWith(record);
    });
  });
});

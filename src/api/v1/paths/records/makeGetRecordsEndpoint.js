// NOTE: since the 3 GET /records/... endpoints are so similar
// we use this helper to generate functions to be exported from
// their respective path modules

const { decapitalize } = require("../../../../util/formatting");
const { SORT_DIRECTIONS } = require("../../../../lib/constants");
const { Asc } = SORT_DIRECTIONS;

const makeGetRecordsEndpoint = (field) => (recordsService) => {
  const GET = (req, res) => {
    const props = {
      ...req.query,
      sortBy: field,
      sortDirection: Asc,
    };

    recordsService.getRecords(props).either(
      (msg) => res.status(400).json(msg),
      (paginatedItems) => res.status(200).json(paginatedItems)
    );
  };

  const lowerField = decapitalize(field);

  GET.apiDoc = {
    summary: `Records sorted by ${lowerField}`,
    operationId: `getRecordsBy${field}`,
    tags: ["record"],
    parameters: [
      {
        name: "page",
        description: "Page number used for paginating items. Default: 1",
        in: "path",
        required: false,
        schema: {
          type: "integer",
        },
        example: 1,
      },
      {
        name: "perPage",
        description:
          "Number of items used per-page when paginating. Default: 100",
        in: "path",
        required: false,
        schema: {
          type: "integer",
        },
        example: 100,
      },
    ],
    responses: {
      200: {
        description: `Records sorted by ${lowerField}`,
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Records",
            },
          },
        },
      },
    },
  };

  return {
    GET,
  };
};

module.exports = makeGetRecordsEndpoint;

const getEndpointTests = require("./commonTests.test");
const endpoint = require("./name");
const { FIELDS: F } = require("../../../../lib/constants");

getEndpointTests({ endpoint, field: F.LastName });

const { FIELDS: F } = require("../../../../lib/constants");
const makeGetRecordsEndpointFor = require("./makeGetRecordsEndpoint");

module.exports = makeGetRecordsEndpointFor(F.Email);

const R = require("ramda");
const { expect } = require("chai");
const { fake } = require("sinon");
const { mockRequest, mockResponse } = require("mock-req-res");
const Result = require("crocks/Result");
const { Err, Ok } = Result;

const { SORT_DIRECTIONS } = require("../../../../lib/constants");
const { Asc } = SORT_DIRECTIONS;
const { makeRecord } = require("../../../../lib/dataGeneration");
const { decapitalize, lowerKeys } = require("../../../../util/formatting");

const commonTests = ({ endpoint, field }) => {
  const lowerField = decapitalize(field);

  describe(`GET /records/${lowerField}`, () => {
    it("calls getRecords", () => {
      const getRecords = fake.returns(Ok("ok"));
      const recordsService = {
        getRecords,
      };

      const props = {
        sortBy: field,
        sortDirection: Asc,
      };

      const req = mockRequest();
      const res = mockResponse();

      const subject = endpoint(recordsService);
      subject.GET(req, res);

      expect(getRecords).to.have.been.calledWith(props);
    });

    it("returns 400 status when getRecords fails", () => {
      const getRecords = fake.returns(Err("oh no!"));
      const recordsService = {
        getRecords,
      };

      const req = mockRequest();
      const res = mockResponse();

      const subject = endpoint(recordsService);
      subject.GET(req, res);

      expect(res.status).to.have.been.calledWith(400);
    });

    describe("when addRecord succeeds", () => {
      it("returns 200 status", () => {
        const getRecords = fake.returns(Ok("ok"));
        const recordsService = {
          getRecords,
        };

        const req = mockRequest();
        const res = mockResponse();

        const subject = endpoint(recordsService);
        subject.GET(req, res);

        expect(res.status).to.have.been.calledWith(200);
      });

      it("returns json containing records as items", () => {
        const items = R.times(() => lowerKeys(makeRecord()), 3);
        const getRecords = fake.returns(Ok({ items }));
        const recordsService = {
          getRecords,
        };

        const req = mockRequest();
        const res = mockResponse();

        const subject = endpoint(recordsService);
        subject.GET(req, res);

        expect(res.json).to.have.been.calledWith({ items });
      });
    });
  });
};

module.exports = commonTests;

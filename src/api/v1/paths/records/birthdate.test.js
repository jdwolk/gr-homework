const getEndpointTests = require("./commonTests.test");
const endpoint = require("./birthdate");
const { FIELDS: F } = require("../../../../lib/constants");

getEndpointTests({ endpoint, field: F.DateOfBirth });

const getEndpointTests = require("./commonTests.test");
const endpoint = require("./email");
const { FIELDS: F } = require("../../../../lib/constants");

getEndpointTests({ endpoint, field: F.Email });

const { lowerKeys } = require("../../../util/formatting");

const RecordsPaths = (recordsService) => {
  const POST = (req, res) => {
    recordsService.addRecord(req.body).either(
      (msg) => res.status(400).json(msg),
      (record) => res.status(201).json(lowerKeys(record))
    );
  };

  POST.apiDoc = {
    summary: "A a single data line in any of the 3 supported record formats",
    operationId: "addRecord",
    tags: ["record"],
    requestBody: {
      required: true,
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/CreateRecord",
          },
        },
      },
    },
    responses: {
      201: {
        description: "Record parsed + created successfully",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/Record",
            },
          },
        },
      },
      400: {
        description: "Record could not be created",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/CreateRecordFailed",
            },
          },
        },
      },
    },
  };

  return {
    POST,
  };
};

module.exports = RecordsPaths;

const R = require("ramda");

const ParseRecords = require("../lib/recordParsing");
const Reports = require("../lib/reports");

const runReport = ({ name, title, reportFn }) => {
  console.log("######################");
  console.log(`${name}`);
  console.log(title);
  console.log("######################");
  console.log(JSON.stringify(reportFn(), null, 2));
  console.log(`#### End ${name} ####\n`);
};

const runReports = () => {
  const { FILES } = process.env;

  if (!FILES) {
    console.log("Example usage:\n");
    console.log(
      'FILES="./data/comma-separated.csv,./data/pipe-separated.csv,./data/space-separated.csv" yarn report'
    );
    return;
  }

  const files = R.map((f) => f.replaceAll(" ", ""), FILES.split(","));
  const records = ParseRecords.fromFiles(files);

  runReport({
    name: "Output 1",
    title: "Sorted by email (descending). Then by last name ascending",
    reportFn: () => Reports.byEmailDescAndLastNameAsc(records),
  });

  runReport({
    name: "Output 2",
    title: "Sorted by birth date, ascending",
    reportFn: () => Reports.byDateOfBirthAsc(records),
  });

  runReport({
    name: "Output 3",
    title: "Sorted by last name, descending",
    reportFn: () => Reports.byLastNameDesc(records),
  });
};

runReports();

const fs = require("fs");

const { makeFileData } = require("../lib/dataGeneration");
const { DELIMITERS } = require("../lib/constants");

const generateData = () => {
  const { NUM_ROWS, FILE_NAME, DELIMITER } = process.env;

  const numRows = NUM_ROWS || 100;
  const fileName = FILE_NAME || "./data/data";
  const delimiter = DELIMITER || DELIMITERS.comma;

  console.log(
    `Generating ${fileName} (numRows: ${numRows}, delimiter: "${delimiter}")`
  );
  fs.promises.writeFile(fileName, makeFileData({ numRows, delimiter }));

  console.log(`Done generating ${fileName}`);
};

generateData();

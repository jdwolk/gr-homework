const R = require("ramda");

const { FIELDS: F, KEY_SEPARATOR } = require("./constants");

// recordKey :: Record -> RecordKeyString
const recordKey = ({
  [F.FirstName]: firstName,
  [F.LastName]: lastName,
  [F.Email]: email,
}) => {
  return R.join(KEY_SEPARATOR, [firstName, lastName, email]);
};

// NOTE: 'Records' is a convenience data type that wraps a collection of
// records. It allows for easy merging (it's a monoid, incidentally) +
// sorting using the SortBy data type.
//
const Records = (keyedRecordsObj) => ({
  // values :: Records ~> [Record]
  values: () => R.values(keyedRecordsObj),

  // kvps :: Records ~> { RecordKeyString: Record }
  kvps: keyedRecordsObj,

  // append :: Records ~> Records -> Records
  append: (otherRecords) =>
    Records({ ...keyedRecordsObj, ...otherRecords.kvps }),

  // addRecord :: Records ~> Record -> Records
  addRecord: (record) => {
    return Records(keyedRecordsObj).append(Records.fromArray([record]));
  },

  // sortValues :: Records ~> [SortBy] -> [Record]
  sortValues: (sortBys) =>
    R.sortWith(
      R.map((s) => s.sorter(), sortBys),
      R.values(keyedRecordsObj)
    ),
});

//      fromArray :: [Record] -> Records
Records.fromArray = (recordsArray) =>
  Records(R.zipObj(R.map(recordKey, recordsArray), recordsArray));

//      empty :: Records
Records.empty = () => Records({});

//      merge :: [Records] -> Records
Records.merge = (recordsCollections) => {
  return R.reduce(
    (records, otherRecords) => records.append(otherRecords),
    Records.empty(),
    recordsCollections
  );
};

//      mergeArrays :: [[Record]] -> Records
Records.mergeArrays = (recordsArrays) => {
  return Records.merge(R.map(Records.fromArray, recordsArrays));
};

Records.addRecord = (record) => (records) => {
  return records.addRecord(record);
};

module.exports = {
  Records,
};

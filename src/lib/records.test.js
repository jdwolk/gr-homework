const R = require("ramda");
const { expect } = require("chai");

const { Records } = require("./records");
const { SortBy } = require("./sortBy");
const { FIELDS: F, SORT_DIRECTIONS } = require("../lib/constants");
const { makeRecord } = require("../lib/dataGeneration");

const { Asc, Desc } = SORT_DIRECTIONS;

const emailsFrom = R.map(R.prop(F.Email));

describe("records", () => {
  describe("Records", () => {
    describe(".fromArray", () => {
      it("creates a Records collection from an array of records", () => {
        const input = R.times(makeRecord, 3);
        const result = Records.fromArray(input);

        const actual = R.length(R.values(result.values()));
        const expected = R.length(input);

        expect(actual).to.equal(expected);
      });
    });

    describe(".append", () => {
      it("correctly appends values from one Records collection onto another", () => {
        const input1 = R.times(makeRecord, 3);
        const input2 = R.times(makeRecord, 4);

        const records1 = Records.fromArray(input1);
        const records2 = Records.fromArray(input2);
        const result = records1.append(records2);

        const actual = emailsFrom(R.values(result.values()));
        const expected = emailsFrom(input1.concat(input2));

        expect(actual).to.have.members(expected);
      });

      it("follows the monoid right identity law", () => {
        const first = Records.fromArray([makeRecord()]);
        const second = Records.empty();

        const expected = first.values();
        const actual = first.append(second).values();

        expect(actual).to.eql(expected);
      });

      it("follows the monoid left identity law", () => {
        const first = Records.empty();
        const second = Records.fromArray([makeRecord()]);

        const expected = second.values();
        const actual = first.append(second).values();

        expect(actual).to.eql(expected);
      });

      it("follows the monoid associativity law", () => {
        const first = Records.fromArray([makeRecord()]);
        const second = Records.fromArray([makeRecord()]);
        const third = Records.fromArray([makeRecord()]);

        const firstResult = first.append(second.append(third)).values();
        const secondResult = first.append(second).append(third).values();

        expect(firstResult).to.eql(secondResult);
      });
    });

    describe(".sortValues", () => {
      it("returns correctly sorted values", () => {
        const input = [
          {
            [F.LastName]: "Atreides",
            [F.FirstName]: "Leto",
            [F.Email]: "latreides@example.com",
            [F.FavoriteColor]: "blue",
            [F.DateOfBirth]: new Date("1960-08-07").getTime(),
          },
          {
            [F.LastName]: "Atreides",
            [F.FirstName]: "Paul",
            [F.Email]: "patreides@example.com",
            [F.FavoriteColor]: "lightblue",
            [F.DateOfBirth]: new Date("1992-01-02").getTime(),
          },
          {
            [F.LastName]: "Jessica",
            [F.FirstName]: "Lady",
            [F.Email]: "ljessica@example.com",
            [F.FavoriteColor]: "orange",
            [F.DateOfBirth]: new Date("1972-01-02").getTime(),
          },
          {
            [F.LastName]: "Harkonnen",
            [F.FirstName]: "Vladimir",
            [F.Email]: "vharkonnen@example.com",
            [F.FavoriteColor]: "red",
            [F.DateOfBirth]: new Date("1972-01-02").getTime(),
          },
        ];

        const result = Records.fromArray(input).sortValues([
          SortBy(F.LastName, Asc),
          SortBy(F.FirstName, Desc),
        ]);

        const actual = R.values(
          R.map(R.compose(R.values, R.pick([F.LastName, F.FirstName])), result)
        );
        const expected = [
          ["Atreides", "Paul"],
          ["Atreides", "Leto"],
          ["Harkonnen", "Vladimir"],
          ["Jessica", "Lady"],
        ];

        expect(actual).to.eql(expected);
      });
    });

    describe(".merge", () => {
      it("merges all records from given Records collections", () => {
        const input1 = R.times(makeRecord, 3);
        const input2 = R.times(makeRecord, 4);
        const input3 = R.times(makeRecord, 4);
        const allInputs = [input1, input2, input3];
        const recordsCollections = R.map(Records.fromArray, allInputs);
        const result = Records.merge(recordsCollections);

        const actual = emailsFrom(result.values());
        const expected = emailsFrom(input1.concat(input2).concat(input3));

        expect(actual).to.eql(expected);
      });

      it("is equivalent to monoid `concat` (ie: `foldr (append) empty`)", () => {
        const firstInput = makeRecord();
        const secondInput = makeRecord();
        const thirdInput = makeRecord();
        const fourthInput = makeRecord();

        const first = Records.fromArray([firstInput]);
        const second = Records.fromArray([secondInput, thirdInput]);
        const third = Records.fromArray([fourthInput]);

        const expected = Records.fromArray([
          firstInput,
          secondInput,
          thirdInput,
          fourthInput,
        ]).values();
        const actual = Records.merge([first, second, third]).values();

        expect(actual).to.eql(expected);
      });
    });

    describe(".mergeArrays", () => {
      it("works the same as .merge but for arrays instead of Records collections", () => {
        const input1 = R.times(makeRecord, 3);
        const input2 = R.times(makeRecord, 4);
        const input3 = R.times(makeRecord, 4);
        const allInputs = [input1, input2, input3];
        const result = Records.mergeArrays(allInputs);

        const actual = emailsFrom(result.values());
        const expected = emailsFrom(input1.concat(input2).concat(input3));

        expect(actual).to.eql(expected);
      });
    });
  });
});

const R = require("ramda");
const D = require("date-fns");

const { dateFormats } = require("../util/formatting");
const { SortBy } = require("./sortBy");
const { FIELDS: F, SORT_DIRECTIONS } = require("./constants");

const { Asc, Desc } = SORT_DIRECTIONS;

//    format :: Record -> Record
const format = (record) => {
  return {
    ...record,
    [F.DateOfBirth]: D.format(
      new Date(Number(record[F.DateOfBirth])),
      dateFormats.default
    ),
  };
};

//    report :: [SortBy] -> Records -> [Record]
const report = (sortBys) => (records) =>
  R.map(format, records.sortValues(sortBys));

//    byEmailDescAndLastNameAsc :: Records -> [Record]
const byEmailDescAndLastNameAsc = report([
  SortBy(F.Email, Desc),
  SortBy(F.LastName, Asc),
]);

//    byDateOfBirthAsc :: Records -> [Record]
const byDateOfBirthAsc = report([SortBy(F.DateOfBirth, Asc, Number)]);

//    byLastNameDesc :: Records -> [Record]
const byLastNameDesc = report([SortBy(F.LastName, Desc)]);

module.exports = {
  byEmailDescAndLastNameAsc,
  byDateOfBirthAsc,
  byLastNameDesc,
};

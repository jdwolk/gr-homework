const R = require("ramda");

const { SORT_DIRECTIONS } = require("./constants");

//    directionFnFrom :: SortDirection -> ((a -> b) -> a -> a -> Number)
const directionFnFrom = (direction) => {
  return direction === SORT_DIRECTIONS.Asc ? R.ascend : R.descend;
};

// NOTE: R.sortWith takes a list of comparators (usually R.ascend/R.descend with some prop accessor function), ie:
//
// const ageNameSort = R.sortWith([
//   R.descend(R.prop('age')),
//   R.ascend(R.prop('name'))
// ]);
//
// SortBy is a convenience data type for making those comparators +
// allows passing an arbitrary function in case the
// data needs to be transformed before sorting (like w/ dates)
//
const SortBy = (field, direction, fieldFn = R.identity) => ({
  // sorter :: SortBy ~> () -> (Any, Any) -> Number
  sorter: () => {
    const directionFn = directionFnFrom(direction);
    return directionFn((val) => fieldFn(R.prop(field, val)));
  },
});

module.exports = {
  SortBy,
};

const R = require("ramda");
const { expect } = require("chai");

const Reports = require("./reports");
const { Records } = require("./records");
const { FIELDS: F } = require("../lib/constants");

const comparableFields = (recordsArray) =>
  R.values(
    R.map(
      R.compose(R.values, R.pick([F.FirstName, F.LastName, F.Email])),
      recordsArray
    )
  );

const makeInput = () => {
  return [
    {
      [F.LastName]: "Atreides",
      [F.FirstName]: "Leto",
      [F.Email]: "caladan4ever@example.com",
      [F.FavoriteColor]: "blue",
      [F.DateOfBirth]: new Date("1960-08-07").getTime(),
    },
    {
      [F.LastName]: "Atreides",
      [F.FirstName]: "Paul",
      [F.Email]: "muaddib@example.com",
      [F.FavoriteColor]: "lightblue",
      [F.DateOfBirth]: new Date("1992-01-02").getTime(),
    },
    {
      [F.LastName]: "Jessica",
      [F.FirstName]: "Lady",
      [F.Email]: "beneGesserit.witch@example.com",
      [F.FavoriteColor]: "orange",
      [F.DateOfBirth]: new Date("1972-01-02").getTime(),
    },
    {
      [F.LastName]: "Harkonnen",
      [F.FirstName]: "Vladimir",
      [F.Email]: "ihatetheatreides@example.com",
      [F.FavoriteColor]: "red",
      [F.DateOfBirth]: new Date("1952-01-02").getTime(),
    },
    {
      [F.LastName]: "Halleck",
      [F.FirstName]: "Gurney",
      [F.Email]: "ballisetLvr@example.com",
      [F.FavoriteColor]: "white",
      [F.DateOfBirth]: new Date("1962-01-02").getTime(),
    },
    {
      [F.LastName]: "Hawat",
      [F.FirstName]: "Thufir",
      [F.Email]: "mentat.eyebrows@example.com",
      [F.FavoriteColor]: "gold",
      [F.DateOfBirth]: new Date("1955-11-12").getTime(),
    },
  ];
};

describe("reports", () => {
  describe(".byEmailDescAndLastNameAsc", () => {
    it("correctly sorts the input records", () => {
      const input = Records.fromArray(makeInput());
      const result = Reports.byEmailDescAndLastNameAsc(input);

      const actual = comparableFields(result);
      const expected = [
        ["Paul", "Atreides", "muaddib@example.com"],
        ["Thufir", "Hawat", "mentat.eyebrows@example.com"],
        ["Vladimir", "Harkonnen", "ihatetheatreides@example.com"],
        ["Leto", "Atreides", "caladan4ever@example.com"],
        ["Lady", "Jessica", "beneGesserit.witch@example.com"],
        ["Gurney", "Halleck", "ballisetLvr@example.com"],
      ];

      expect(expected).to.eql(actual);
    });
  });

  describe(".byDateOfBirthAsc", () => {
    it("correctly sorts the input records", () => {
      const input = Records.fromArray(makeInput());
      const result = Reports.byDateOfBirthAsc(input);

      const actual = comparableFields(result);
      const expected = [
        ["Vladimir", "Harkonnen", "ihatetheatreides@example.com"],
        ["Thufir", "Hawat", "mentat.eyebrows@example.com"],
        ["Leto", "Atreides", "caladan4ever@example.com"],
        ["Gurney", "Halleck", "ballisetLvr@example.com"],
        ["Lady", "Jessica", "beneGesserit.witch@example.com"],
        ["Paul", "Atreides", "muaddib@example.com"],
      ];

      expect(expected).to.eql(actual);
    });
  });

  describe(".byLastNameDesc", () => {
    it("correctly sorts the input records", () => {
      const input = Records.fromArray(makeInput());
      const result = Reports.byLastNameDesc(input);

      const actual = comparableFields(result);
      const expected = [
        ["Lady", "Jessica", "beneGesserit.witch@example.com"],
        ["Thufir", "Hawat", "mentat.eyebrows@example.com"],
        ["Vladimir", "Harkonnen", "ihatetheatreides@example.com"],
        ["Gurney", "Halleck", "ballisetLvr@example.com"],
        ["Leto", "Atreides", "caladan4ever@example.com"],
        ["Paul", "Atreides", "muaddib@example.com"],
      ];

      expect(expected).to.eql(actual);
    });
  });
});

const NEWLINE_CHAR = "\n";

const DELIMITERS = {
  comma: ",",
  pipe: "|",
  space: " ",
};

const KEY_SEPARATOR = "::";

const FIELDS = {
  LastName: "LastName",
  FirstName: "FirstName",
  Email: "Email",
  FavoriteColor: "FavoriteColor",
  DateOfBirth: "DateOfBirth",
};

const SORT_DIRECTIONS = {
  Asc: "Asc",
  Desc: "Desc",
};

module.exports = {
  DELIMITERS,
  FIELDS,
  KEY_SEPARATOR,
  NEWLINE_CHAR,
  SORT_DIRECTIONS,
};

const R = require("ramda");
const fs = require("fs");

const { DELIMITERS } = require("./constants");
const { syncParse } = require("../util/parsing");
const { Records } = require("./records");

const _doesDelimiterWork = ({ contents: givenContents, delimiter }) => {
  const line = R.take(1, givenContents.split("\n")).join("\n");
  const includesDelimiter = R.includes(delimiter, line);
  return includesDelimiter;
};

const doesDelimiterWork = (contents) => (delimiter) => {
  return _doesDelimiterWork({ contents, delimiter });
};

const determineDelimiterType = (contents) => {
  const canParse = doesDelimiterWork(contents);

  switch (true) {
    case canParse(DELIMITERS.comma):
      return DELIMITERS.comma;
    case canParse(DELIMITERS.pipe):
      return DELIMITERS.pipe;
    case canParse(DELIMITERS.space):
      return DELIMITERS.space;
    default:
      return null;
  }
};

const parse = (contents) => {
  const delimiter = determineDelimiterType(contents);
  if (!delimiter) {
    return null;
  }

  try {
    return syncParse({ contents, delimiter });
  } catch {
    return null;
  }
};

const fromFiles = (files) => {
  const filesContents = R.map((file) => fs.readFileSync(file, "utf8"), files);
  const recordArrays = R.map(parse, filesContents);
  return Records.mergeArrays(recordArrays);
};

module.exports = {
  determineDelimiterType,
  parse,
  fromFiles,
};

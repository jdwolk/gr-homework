const R = require("ramda");
const fs = require("fs");
const { expect } = require("chai");

const RP = require("./recordParsing");
const { DELIMITERS, FIELDS } = require("./constants");
const { dataFile } = require("../util/paths");

const readFile = (file) => {
  return fs.readFileSync(file, "utf8");
};

describe("recordParsing", () => {
  describe(".parse", () => {
    it("correctly reads all records from the given file", () => {
      const contents = readFile(dataFile("comma-separated.csv"));
      const result = RP.parse(contents);

      const hasEmptyLastNameField = R.pipe(R.isNil, R.prop(FIELDS.LastName));

      const noneHaveEmptyLastName = R.none(hasEmptyLastNameField, result);

      expect(R.length(result)).to.be.above(0);
      expect(noneHaveEmptyLastName).to.equal(true);
    });
  });

  describe(".determineDelimiterType", () => {
    describe("given a file delimited by commas", () => {
      it("returns the comma delimiter", () => {
        const contents = readFile(dataFile("comma-separated.csv"));
        const result = RP.determineDelimiterType(contents);

        expect(result).to.equal(DELIMITERS.comma);
      });
    });

    describe("given a file delimited by pipes", () => {
      it("returns the pipe delimiter", () => {
        const contents = readFile(dataFile("pipe-separated.csv"));
        const result = RP.determineDelimiterType(contents);

        expect(result).to.equal(DELIMITERS.pipe);
      });
    });

    describe("given a file delimited by spaces", () => {
      it("returns the space delimiter", () => {
        const contents = readFile(dataFile("space-separated.csv"));
        const result = RP.determineDelimiterType(contents);

        expect(result).to.equal(DELIMITERS.space);
      });
    });
  });
});

const faker = require("faker");
const R = require("ramda");

const { NEWLINE_CHAR, FIELDS } = require("./constants");

const F = FIELDS;

const makeRecord = () => {
  return {
    [F.LastName]: faker.name.lastName(),
    [F.FirstName]: faker.name.firstName(),
    [F.Email]: faker.internet.email(),
    [F.FavoriteColor]: faker.vehicle.color().replaceAll(" ", ""), // some colors have spaces
    // uses timestamp (# milliseconds) so we don't have to deal with spaces
    [F.DateOfBirth]: String(
      faker.date.between("1971-01-01", "2021-04-17").getTime()
    ),
  };
};

const toRowString = ({ row, delimiter }) => {
  return R.join(delimiter, R.values(row));
};

const makeFileData = ({ numRows, delimiter }) => {
  const headers = FIELDS;
  const rowsData = R.times(makeRecord, numRows);
  const allData = R.concat([headers], rowsData);
  const allRows = R.map((row) => toRowString({ row, delimiter }), allData);
  return R.join(NEWLINE_CHAR, allRows);
};

module.exports = {
  makeRecord,
  makeFileData,
};

# JD Wolk's Homework Assignment for Guaranteed Rate

This app is a [node.js](https://nodejs.org/en/) / [express](https://expressjs.com/) web + CLI app written in JavaScript.

(I don't particularly like node but the stack is pretty easy to get up and running quickly)

## Requirements

* [node.js](https://nodejs.org/en/) >= 15.14.0 (any recent node _should_ work though)
  * [asdf](https://github.com/asdf-vm/asdf) is a really handy cross-language version manager. I highly recommend it for managing, eg, node versions
  * [asdf-nodejs](https://github.com/asdf-vm/asdf-nodejs) is the plugin you'd use with asdf to install node
* [yarn](https://yarnpkg.com/) >= 1.22.4

## Setup

```
$ yarn install
```

## Running

### Data Generator

You can generate data files for the CLI app. Args are passed as env vars:

* `FILE_NAME`: path to the to-be-generated file. Defaults to './data/data'
* `NUM_ROWS`: the # of rows to be generated in the file. Defaults to 100
* `DELIMITER`: delimiter to use for separating values within the file. Defaults to ','

Ex:
```
$ NUM_ROWS=10 FILE_NAME="./data/comma-delimited.csv" DELIMITER="," yarn gen-data
```

### CLI App

The `yarn report` script generates the report with the sorted outputs.

Args are passed as env vars:

* `FILES`: (required) comma-separated list of records which will be merged, sorted and printed

Ex:
```
$ FILES="./data/comma-separated.csv,./data/pipe-separated.csv,./data/space-separated.csv" yarn report
```

### Web Server

Run normally:

```
$ yarn start
```

To run with debug output + reloading (NOTE: the DB is in-memory so any code changes will reset it when the server is reloaded)

```
$ yarn dev
```

If you pass the `SEED_RECORDS` env var the in-memory DB will be seeded with records from `./data/comma-separated.csv`:
```
$ SEED_RECORDS=true yarn dev
```

You can control the port with the `PORT` env var if for some reason you don't want to run on `3000`, ie:

```
$ PORT=1337 yarn start
```

### API Docs

When running the server the api docs (OpenAPI 3.0) will be available at `localhost:3000/api-documentation`.

An OpenAPI-compatible JSON schema can be found at `localhost:3000/api-docs`

## Linting

Linting config is in `.eslintrc.json`.

You can lint the codebase with:

```
$ yarn lint
```

## Tests

Run tests:

```
$ yarn test
```

Or to continuously run tests:

```
$yarn test-watch
```

Generate a code coverage report (HTML report will log to `./coverage/index.html`):

```
$ yarn coverage
```

